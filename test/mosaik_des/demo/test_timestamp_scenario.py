from mosaik_zero.zero.mosaik_des.demo.scenario.timestamp_source_sink_scenario \
    import TimestampScenario


def test_timestamp_scenario():
    TimestampScenario()(limit_steps=100_000)
