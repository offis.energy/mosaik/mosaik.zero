from mosaik_zero.zero.mosaik_zero.demo.scenario.datetime_transducer_scenario \
    import DatetimeTransducerScenario


def test_datetime_transducer_scenario():
    DatetimeTransducerScenario()(limit_steps=2)
