from mosaik_zero.zero.mosaik_zero.demo.scenario.datetime_splitter_scenario \
    import DatetimeSplitterScenario


def test_datetime_splitter_scenario():
    DatetimeSplitterScenario()(limit_steps=2)
