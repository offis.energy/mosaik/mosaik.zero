from pytest import raises

from mosaik_zero.zero.mosaik_zero.api.component.base_component import \
    BaseComponent


def test_simulator_component():
    # Assert
    with raises(TypeError):
        # Test
        _ = BaseComponent()
