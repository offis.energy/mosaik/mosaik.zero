from pytest import raises

from mosaik_zero.zero.mosaik_two.demo.not_implemented.\
    not_implemented_simulator import NotImplementedSimulator
from mosaik_zero.zero.mosaik_two.demo.not_implemented.\
    not_implemented_simulator_meta import \
    NOT_IMPLEMENTED_SIMULATOR_META


def test_not_implemented_simulator():
    # Mock
    meta = NOT_IMPLEMENTED_SIMULATOR_META

    # Test
    _ = NotImplementedSimulator(meta=meta)


def test_not_implemented_simulator_create():
    # Mock
    meta = NOT_IMPLEMENTED_SIMULATOR_META
    not_implemented_simulator = NotImplementedSimulator(meta=meta)
    num: int = 0
    model: str = ''
    model_params: dict = {}

    # Test
    with raises(NotImplementedError):
        not_implemented_simulator.create(
            num=num, model=model, model_params=model_params,
        )


def test_not_implemented_simulator_step():
    # Mock
    meta = NOT_IMPLEMENTED_SIMULATOR_META
    not_implemented_simulator = NotImplementedSimulator(meta=meta)
    time: int = 1
    inputs: dict = {}

    # Test
    with raises(NotImplementedError):
        not_implemented_simulator.step(
            time=time,
            inputs=inputs,
        )


def test_not_implemented_simulator_get_data():
    # Mock
    meta = NOT_IMPLEMENTED_SIMULATOR_META
    not_implemented_simulator = NotImplementedSimulator(meta=meta)
    outputs: dict = {}

    # Test
    with raises(NotImplementedError):
        not_implemented_simulator.get_data(
            outputs=outputs,
        )
