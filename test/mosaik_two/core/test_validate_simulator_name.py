from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_simulator_name_module import \
    validate_simulator_name


def test_validate_simulator_name():
    # Mock
    sim_config: dict = {}
    simulator_name: str = ""

    # Test
    validate_simulator_name(
        simulator_name=simulator_name,
        sim_config=sim_config,
    )


def test_validate_simulator_name_type_error():
    # Mock
    sim_config: type(None) = None
    simulator_name: type(None) = None

    # Test
    with raises(TypeError):
        validate_simulator_name(
            simulator_name=simulator_name,
            sim_config=sim_config,
        )
