from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_time_to_next_step_module import \
    validate_next_step_at


def test_validate_next_step_at():
    # Mock
    next_step_at: int = 2
    step: int = 1

    # Test
    validate_next_step_at(
        next_step_at=next_step_at,
        step=step,
    )


def test_validate_next_step_at_too_small():
    # Mock
    next_step_at: int = 1
    step: int = 2

    # Test
    with raises(ValueError):
        validate_next_step_at(
            next_step_at=next_step_at,
            step=step,
        )


def test_validate_next_step_at_zero():
    # Mock
    next_step_at: int = 0
    step: int = -1

    # Test
    with raises(ValueError):
        validate_next_step_at(
            next_step_at=next_step_at,
            step=step,
        )


def test_validate_next_step_at_type_error():
    # Mock
    step: type(None) = None
    next_step_at: type(None) = None

    # Test
    with raises(TypeError):
        validate_next_step_at(
            next_step_at=next_step_at,
            step=step,
        )
