from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_attr_pairs_module import \
    validate_attr_pairs


def test_validate_attr_pairs():
    # Mock
    attr_pairs = ()

    # Test
    validate_attr_pairs(attr_pairs=attr_pairs)


def test_validate_attr_pairs_type_error():
    # Mock
    attr_pairs: type(None) = None

    # Test
    with raises(TypeError):
        validate_attr_pairs(attr_pairs=attr_pairs)
