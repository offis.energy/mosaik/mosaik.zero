from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_sim_config_module import \
    validate_sim_config


def test_validate_sim_config():
    # Mock
    sim_config: dict = {}

    # Test
    validate_sim_config(sim_config=sim_config)


def test_validate_sim_config_type_error():
    # Mock
    sim_config: type(None) = None

    # Test
    with raises(TypeError):
        validate_sim_config(sim_config=sim_config)
