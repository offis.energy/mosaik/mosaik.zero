from pytest import raises

from mosaik_zero.zero.mosaik_two.api.entity import Entity
from mosaik_zero.zero.mosaik_two.core.validate_entity_module \
    import validate_entity


def test_validate_entity():
    # Mock
    entity = Entity()

    # Test
    validate_entity(entity=entity)


def test_validate_entity_type_error():
    # Mock
    entity: type(None) = None

    # Test
    with raises(TypeError):
        validate_entity(entity=entity)
