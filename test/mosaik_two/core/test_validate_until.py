from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_until_module \
    import validate_until


def test_validate_until():
    # Mock
    until = 1

    # Test
    validate_until(until=until)


def test_validate_until_type_error():
    # Mock
    until: type(None) = None

    # Test
    with raises(TypeError):
        validate_until(until=until)
