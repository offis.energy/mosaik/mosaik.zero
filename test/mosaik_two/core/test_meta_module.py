from pytest import raises

from mosaik_zero.zero.mosaik_two.core.validate_meta_module import validate_meta


def test_validate_meta():
    # Mock
    meta: dict = {
        'models': {}
    }

    # Test
    validate_meta(meta=meta)


def test_validate_meta_type_error():
    # Mock
    meta: type(None) = None

    # Test
    with raises(TypeError):
        validate_meta(meta=meta)


def test_validate_meta_too_long():
    # Mock
    meta: dict = {
        'models': {
            0: {},
            1: {},
        }
    }

    # Test
    with raises(NotImplementedError):
        validate_meta(meta=meta)
