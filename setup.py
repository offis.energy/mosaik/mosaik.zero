import datetime

from setuptools import setup, find_namespace_packages

NAMESPACE = 'mosaik_zero'
PACKAGE = 'zero'

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

setup(
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    cmdclass={
    },
    description='A toy implementation of mosaik from scratch.',
    entry_points={
    },
    include_package_data=True,
    install_requires=[
    ],
    long_description=(
            open('README.md').read()
    ),
    long_description_content_type='text/markdown',
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    name='mosaik' + '.' + 'zero',
    packages=find_namespace_packages(include=[NAMESPACE + '.*']),
    package_dir={'': '.'},
    setup_requires=[
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
    ],
    url='https://gitlab.com/offis.energy/mosaik/mosaik.zero',
    version='0.1.0' + 'rc' + TIMESTAMP,
    zip_safe=False,
)
