from mosaik_zero.zero.mosaik_zero.core.simulate_step_module import \
    simulate_step


def simulate(
    *,
    connections,
    limit_steps=None,
):
    """
    Simulate the given connections until the step limits is reached.

    :param connections:
    :param limit_steps:
    :return:
    """
    step = 0
    while True:
        step += 1
        if step > limit_steps:
            break

        simulate_step(connections=connections, step=step)
