from typing import Mapping


class Connection(Mapping):
    def __init__(
        self,
        *,
        source_component,
        sink_component,
        source_provision,
        sink_requirement,
    ):
        self._connection = {
            'source': {
                source_component: source_provision,
            },
            'sink': {
                sink_component: sink_requirement,
            },
        }

    def __getitem__(self, key):
        return self._connection[key]

    def __iter__(self):
        return iter(self._connection)

    def __len__(self):
        return len(self._connection)
