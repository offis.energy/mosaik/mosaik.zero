from abc import abstractmethod


class BaseScenario(object):
    @abstractmethod
    def __call__(
        self,
        *,
        limit_steps,
    ):
        pass
