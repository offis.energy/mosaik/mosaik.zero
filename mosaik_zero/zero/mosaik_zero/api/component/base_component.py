from abc import ABC, abstractmethod


class BaseComponent(ABC):
    def __init__(self):
        pass

    @property
    @abstractmethod
    def requirements(self) -> tuple:
        pass

    @property
    @abstractmethod
    def provisions(self) -> tuple:
        pass
