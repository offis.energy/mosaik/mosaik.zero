from abc import ABC, abstractmethod

from mosaik_zero.zero.mosaik_zero.api.component.base_component import \
    BaseComponent


class SourceComponent(BaseComponent, ABC):
    @property
    def requirements(self):
        return ()

    @abstractmethod
    def __call__(
        self,
        step,
    ) -> dict:
        """
        Create outputs for the given step.

        :param step: The simulation step to create the outputs for.
        :return: The output for this simulation step.
        """
        pass
