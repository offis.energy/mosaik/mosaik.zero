from abc import abstractmethod

from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent
from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


class FlowComponent(SinkComponent, SourceComponent):
    @abstractmethod
    def __call__(self, step, inputs) -> dict:
        """
        Calculate the outputs for the given step from the given inputs.

        :param step: The simulation step to derive the outputs for.
        :return: The output for this simulation step.
        """
        pass
