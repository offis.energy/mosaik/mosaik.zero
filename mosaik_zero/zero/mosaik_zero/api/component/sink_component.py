from abc import ABC, abstractmethod

from mosaik_zero.zero.mosaik_zero.api.component.base_component import \
    BaseComponent


class SinkComponent(BaseComponent, ABC):
    @property
    def provisions(self) -> tuple:
        return ()

    @abstractmethod
    def __call__(
        self,
        step,
        inputs,
    ) -> None:
        """
        Handle the given inputs for the given step.

        :param inputs: The inputs to handle.
        :param step: The simulation step to handle the inputs for.
        :return: None
        """
        pass
