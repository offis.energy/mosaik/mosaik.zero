from typing import Dict, Tuple

from mosaik_zero.zero.mosaik_zero.api.component.base_component import \
    BaseComponent

from mosaik_zero.zero.mosaik_zero.core.\
    components_are_connected_with_requirement_module import \
    components_are_connected_with_requirement


def component_can_be_called(
    *,
    component: BaseComponent,
    components_called: Dict[BaseComponent, bool],
    connections: Tuple[BaseComponent, BaseComponent],
):
    requirements_satisfied: set = set()
    for requirement in component.requirements:
        for source_component, source_called in components_called.items():
            if not source_called:
                continue
            if not components_are_connected_with_requirement(
                source_component, component, requirement, connections
            ):
                continue
            requirements_satisfied.add(requirement)

    can_be_called: bool = all(
        requirement in requirements_satisfied
        for requirement in component.requirements
    )

    return can_be_called
