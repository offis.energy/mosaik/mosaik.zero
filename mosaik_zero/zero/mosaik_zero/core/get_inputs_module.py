from itertools import product

from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent
from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


def get_inputs(
    *,
    component,
    component_outputs,
    connections,
) -> dict:
    inputs: dict = {}

    for connection in connections:
        source: SourceComponent = list(connection['source'].keys())[0]
        sink: SinkComponent = list(connection['sink'].keys())[0]
        if sink is not component:
            continue

        for provision, requirement in \
                product(source.provisions, sink.requirements):
            if provision == requirement:
                # TODO Handle double source
                outputs = component_outputs[source]
                inputs[requirement] = outputs[provision]

    return inputs
