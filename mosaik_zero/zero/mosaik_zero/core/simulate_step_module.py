from frozendict import frozendict

from mosaik_zero.zero.mosaik_zero.core.component_can_be_called_module import \
    component_can_be_called
from mosaik_zero.zero.mosaik_zero.core.get_connected_components_module import \
    get_connected_components
from mosaik_zero.zero.mosaik_zero.core.get_inputs_module import get_inputs


def simulate_step(connections, step):
    components_called = {
        component: False
        for component in get_connected_components(connections)
    }

    component_outputs = {}

    while not all(components_called.values()):
        for component in components_called:
            if component_can_be_called(
                component=component,
                components_called=components_called,
                connections=connections,
            ):
                inputs = get_inputs(
                    component=component,
                    connections=connections,
                    component_outputs=component_outputs,
                )

                if inputs == {}:
                    # print(
                    #     f'Calling component {type(component).__name__} '
                    #     'without any inputs...'
                    # )
                    outputs = component(step=step)
                else:
                    # print(
                    #     f'Calling component {type(component).__name__} '
                    #     f'with inputs {inputs} ...'
                    # )
                    # Freeze inputs to enable hashing them
                    inputs = frozendict(inputs)
                    outputs = component(step, inputs)

                component_outputs[component] = outputs
                components_called[component] = True
