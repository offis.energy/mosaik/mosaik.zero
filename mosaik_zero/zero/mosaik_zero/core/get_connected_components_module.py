def get_connected_components(connections) -> set:
    components: set = set()
    for connection in connections:
        source_component = list(connection['source'].keys())[0]
        components.add(source_component)
        sink_component = list(connection['sink'].keys())[0]
        components.add(sink_component)

    return components
