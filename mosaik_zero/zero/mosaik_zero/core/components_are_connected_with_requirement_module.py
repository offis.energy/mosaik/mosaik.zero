def components_are_connected_with_requirement(
    source_component,
    component,
    requirement,
    connections
) -> bool:
    connected: bool = False

    for connection in connections:
        if list(connection['source'].keys())[0] is not source_component:
            continue
        if list(connection['sink'].keys())[0] is not component:
            continue
        # Don't care how source calls its attribute.
        # So skip checking it here.
        if list(connection['sink'].values())[0] is not requirement:
            continue

        connected = True

    return connected
