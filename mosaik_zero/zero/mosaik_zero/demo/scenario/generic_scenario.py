from mosaik_zero.zero.mosaik_zero.api.scenario.simulate_module import simulate
from mosaik_zero.zero.mosaik_zero.api.scenario.base_scenario import \
    BaseScenario


class GenericScenario(BaseScenario):
    def __init__(self, connections):
        self._connections = connections

    def __call__(
        self,
        limit_steps,
    ):
        simulate(
            connections=self._connections,
            limit_steps=limit_steps,
        )
