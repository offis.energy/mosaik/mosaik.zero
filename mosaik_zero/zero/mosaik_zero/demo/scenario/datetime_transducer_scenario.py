from typing import List

from mosaik_zero.zero.mosaik_zero.api.scenario.connection import Connection
from mosaik_zero.zero.mosaik_zero.demo.component.sink.datetime_sink_component\
    import DatetimeSinkComponent
from mosaik_zero.zero.mosaik_zero.demo.component.source.\
    datetime_source_component import DatetimeSourceComponent
from mosaik_zero.zero.mosaik_zero.demo.component.flow.\
    datetime_transducer_component import DatetimeTransducerComponent
from mosaik_zero.zero.mosaik_zero.demo.scenario.generic_scenario import \
    GenericScenario


class DatetimeTransducerScenario(GenericScenario):
    def __init__(self):
        datetime_source_component: DatetimeSourceComponent = \
            DatetimeSourceComponent()
        datetime_transducer_component: DatetimeTransducerComponent = \
            DatetimeTransducerComponent()
        datetime_sink_component: DatetimeSinkComponent = \
            DatetimeSinkComponent()

        connections: List = [
            Connection(
                source_component=datetime_source_component,
                source_provision='datetime',
                sink_component=datetime_transducer_component,
                sink_requirement='datetime',
            ),
            Connection(
                source_component=datetime_transducer_component,
                source_provision='datetime',
                sink_component=datetime_sink_component,
                sink_requirement='datetime',
            ),
        ]

        super().__init__(connections)
