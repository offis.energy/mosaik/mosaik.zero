from typing import List

from mosaik_zero.zero.mosaik_zero.api.scenario.connection import Connection
from mosaik_zero.zero.mosaik_zero.demo.component.sink.\
    date_and_time_sink_component import \
    DateAndTimeSinkComponent
from mosaik_zero.zero.mosaik_zero.demo.component.source.\
    datetime_source_component import \
    DatetimeSourceComponent
from mosaik_zero.zero.mosaik_zero.demo.component.flow.\
    datetime_splitter_component import DatetimeSplitterComponent
from mosaik_zero.zero.mosaik_zero.demo.scenario.generic_scenario import \
    GenericScenario


class DatetimeSplitterScenario(GenericScenario):
    def __init__(self):
        datetime_source_component: DatetimeSourceComponent = \
            DatetimeSourceComponent()
        datetime_splitter_component: DatetimeSplitterComponent = \
            DatetimeSplitterComponent()
        date_and_time_sink_component: DateAndTimeSinkComponent = \
            DateAndTimeSinkComponent()

        connections: List = [
            Connection(
                source_component=datetime_source_component,
                source_provision='datetime',
                sink_component=datetime_splitter_component,
                sink_requirement='datetime',
            ),
            Connection(
                source_component=datetime_splitter_component,
                source_provision='date',
                sink_component=date_and_time_sink_component,
                sink_requirement='date',
            ),
            Connection(
                source_component=datetime_splitter_component,
                source_provision='time',
                sink_component=date_and_time_sink_component,
                sink_requirement='time',
            ),
        ]

        super().__init__(connections)
