from typing import List

from mosaik_zero.zero.mosaik_zero.api.scenario.connection import Connection
from mosaik_zero.zero.mosaik_zero.demo.component.sink.\
    date_and_time_sink_component import \
    DateAndTimeSinkComponent
from mosaik_zero.zero.mosaik_zero.demo.component.source.date_source_component\
    import DateSourceComponent
from mosaik_zero.zero.mosaik_zero.demo.component.source.time_source_component\
    import TimeSourceComponent
from mosaik_zero.zero.mosaik_zero.demo.scenario.generic_scenario import \
    GenericScenario


class DateAndTimeScenario(GenericScenario):
    def __init__(self):
        date_source_component = DateSourceComponent()
        time_source_component = TimeSourceComponent()
        date_and_time_sink_component = DateAndTimeSinkComponent()
        connections: List = [
            Connection(
                source_component=date_source_component,
                source_provision='date',
                sink_component=date_and_time_sink_component,
                sink_requirement='date',
            ),
            Connection(
                source_component=time_source_component,
                source_provision='time',
                sink_component=date_and_time_sink_component,
                sink_requirement='time',
            ),
        ]

        super().__init__(connections)
