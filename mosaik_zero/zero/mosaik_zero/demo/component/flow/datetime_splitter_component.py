from datetime import datetime, date, time
from functools import lru_cache

from mosaik_zero.zero.mosaik_zero.api.component.flow_component import \
    FlowComponent


class DatetimeSplitterComponent(FlowComponent):

    def __init__(self):
        super().__init__()

        self._datetime_requirement_name = 'datetime'
        self._date_provision_name = 'date'
        self._time_provision_name = 'time'

    @property
    def provisions(self) -> dict:
        provisions: dict = {
            self._date_provision_name: date,
            self._time_provision_name: time,
        }

        return provisions

    @property
    def requirements(self) -> dict:
        requirements: dict = {
            self._datetime_requirement_name: datetime,
        }

        return requirements

    @lru_cache(maxsize=1)
    def __call__(self, step, inputs) -> dict:
        outputs: dict = \
            {
                self._date_provision_name:
                    inputs[self._datetime_requirement_name].date(),
                self._time_provision_name:
                    inputs[self._datetime_requirement_name].time(),
            }

        return outputs
