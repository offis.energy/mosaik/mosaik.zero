from datetime import datetime
from functools import lru_cache

from mosaik_zero.zero.mosaik_zero.api.component.flow_component import \
    FlowComponent


class DatetimeTransducerComponent(FlowComponent):

    def __init__(self):
        super().__init__()

        self._datetime_provision_name = self._datetime_requirement_name = \
            'datetime'

    @property
    def provisions(self) -> dict:
        provisions: dict = {
            self._datetime_provision_name: datetime,
        }

        return provisions

    @property
    def requirements(self) -> dict:
        requirements: dict = {
            self._datetime_requirement_name: datetime,
        }

        return requirements

    @lru_cache(maxsize=1)
    def __call__(self, step, inputs) -> dict:
        outputs: dict = \
            {
                self._datetime_provision_name:
                    inputs[self._datetime_requirement_name]
            }

        return outputs
