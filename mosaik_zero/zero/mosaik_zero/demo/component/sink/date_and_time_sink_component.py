from datetime import date, time

from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent


class DateAndTimeSinkComponent(SinkComponent):

    def __init__(self):
        super().__init__()

        self._date_requirement_name = 'date'
        self._time_requirement_name = 'time'

    @property
    def requirements(self) -> dict:
        requirements: dict = {
            self._date_requirement_name: date,
            self._time_requirement_name: time,
        }

        return requirements

    def __call__(self, step, inputs) -> None:
        print(
            'The date is:', inputs[self._date_requirement_name].isoformat(),
            'and the time is:', inputs[self._time_requirement_name].isoformat()
        )
