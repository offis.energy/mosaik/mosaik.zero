from datetime import datetime

from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent


class DatetimeSinkComponent(SinkComponent):

    def __init__(self):
        super().__init__()

        self._datetime_requirement_name = 'datetime'

    @property
    def requirements(self) -> dict:
        requirements: dict = {
            self._datetime_requirement_name: datetime,
        }

        return requirements

    def __call__(self, step, inputs) -> None:
        print(
            'The time is:', inputs[self._datetime_requirement_name].isoformat()
        )
