from datetime import datetime
from functools import lru_cache

from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


class DatetimeSourceComponent(SourceComponent):

    def __init__(self):
        super().__init__()

        self._datetime_provision_name = 'datetime'

    @property
    def provisions(self) -> dict:
        provisions: dict = {
            self._datetime_provision_name: datetime,
        }

        return provisions

    @lru_cache(maxsize=1)
    def __call__(self, step) -> dict:
        outputs: dict = {
            self._datetime_provision_name: datetime.utcnow()
        }

        return outputs
