from datetime import time, datetime
from functools import lru_cache

from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


class TimeSourceComponent(SourceComponent):

    def __init__(self):
        super().__init__()

        self._time_provision_name = 'time'

    @property
    def provisions(self) -> dict:
        provisions: dict = {
            self._time_provision_name: time,
        }

        return provisions

    @lru_cache(maxsize=1)
    def __call__(self, step) -> dict:
        outputs: dict = {
            self._time_provision_name: datetime.utcnow().time()
        }

        return outputs
