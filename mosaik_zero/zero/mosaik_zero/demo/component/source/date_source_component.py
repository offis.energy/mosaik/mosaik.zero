from datetime import date, datetime
from functools import lru_cache

from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


class DateSourceComponent(SourceComponent):

    def __init__(self):
        super().__init__()

        self._date_provision_name = 'date'

    @property
    def provisions(self) -> dict:
        provisions: dict = {
            self._date_provision_name: date,
        }

        return provisions

    @lru_cache(maxsize=1)
    def __call__(self, step) -> dict:
        outputs: dict = {
            self._date_provision_name: datetime.utcnow().date()
        }

        return outputs
