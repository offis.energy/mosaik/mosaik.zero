from typing import List

from mosaik_zero.zero.mosaik_des.demo.component.timestamp_event_sink import \
    TimestampEventSink
from mosaik_zero.zero.mosaik_des.demo.component.timestamp_event_source import \
    TimestampEventSource
from mosaik_zero.zero.mosaik_zero.api.scenario.connection import Connection
from mosaik_zero.zero.mosaik_zero.api.scenario.simulate_module import simulate
from mosaik_zero.zero.mosaik_zero.demo.scenario.generic_scenario import \
    GenericScenario


class TimestampScenario(GenericScenario):
    def __init__(self):
        second_source_component: TimestampEventSource = \
            TimestampEventSource()
        second_sink_component: TimestampEventSink = \
            TimestampEventSink()

        connections: List = [
            Connection(
                source_component=second_source_component,
                source_provision='timestamp',
                sink_component=second_sink_component,
                sink_requirement='timestamp',
            )
        ]

        super().__init__(connections)

    def __call__(
        self,
        limit_steps,
    ):
        simulate(
            connections=self._connections,
            limit_steps=limit_steps,
        )
