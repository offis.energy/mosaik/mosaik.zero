from datetime import datetime, timedelta

from mosaik_zero.zero.mosaik_des.api.component.event_sink import EventSink


class TimestampEventSink(EventSink):
    @property
    def requirements(self) -> dict:
        _requirements: dict = {
            self._timestamp_provision_name: int,
        }

        return _requirements

    def __init__(self):
        super().__init__()

        self._timestamp_provision_name = 'timestamp'
        self._last_second = 0

    def sink_event(self, event):
        value: datetime = event[1].value
        begin = event[1].begin
        begin = begin - timedelta(microseconds=begin.microsecond)
        end = event[1].end
        end = end - timedelta(microseconds=end.microsecond)

        print(
            f'The timestamp was {value} from {begin} till {end}.'
        )
