from datetime import datetime, timedelta

from mosaik_zero.zero.mosaik_des.api.component.event_source import EventSource
from mosaik_zero.zero.mosaik_des.api.data.event import Event


class TimestampEventSource(EventSource):
    def __init__(self):
        super().__init__()

        self._timestamp_provision_name = 'timestamp'
        self._last_timestamp = 0

    @property
    def provisions(self) -> dict:
        _provisions: dict = {
            self._timestamp_provision_name: int,
        }

        return _provisions

    @property
    def events(self) -> dict:
        events: dict = {}

        # Create an event whenever the unix timestamp changes
        timestamp = int(datetime.utcnow().timestamp())
        if timestamp > self._last_timestamp:
            self._last_timestamp = timestamp

            now = datetime.utcnow()
            events[self._timestamp_provision_name] = \
                Event(
                    begin=now,
                    end=now + timedelta(seconds=1),
                    value=timestamp,
                )
        else:
            events[self._timestamp_provision_name] = {}

        return events
