from dataclasses import dataclass
from datetime import datetime
from typing import Any


@dataclass
class Event(object):
    begin: datetime
    end: datetime
    value: Any
