from abc import ABC, abstractmethod
from queue import Queue
from threading import Thread
from time import sleep

from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent


class EventSink(SinkComponent, ABC):
    def __init__(self):
        super().__init__()

        self._events_output_queue: Queue = Queue()
        self._events_input_queue: Queue = Queue()

        def sink_event_target():
            while True:
                if self._events_input_queue.empty():
                    sleep(0.1)
                    continue
                event = self._events_input_queue.get()

                self.sink_event(event)

        Thread(
            target=sink_event_target,
            daemon=True,
        ).start()

    def __call__(
        self,
        step,
        inputs,
    ):
        self.events = inputs

    @abstractmethod
    def sink_event(self, event):
        pass

    @property
    def events(self):

        events: dict = {}
        while not self._events_output_queue.empty():
            events.update(self._events_output_queue.get())

        return events

    @events.setter
    def events(self, events) -> None:
        for event in events.items():
            if event[1] == {}:
                continue

            self._events_input_queue.put(event)
