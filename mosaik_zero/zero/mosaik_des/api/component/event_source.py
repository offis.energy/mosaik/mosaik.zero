from abc import ABC, abstractmethod

from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent


class EventSource(SourceComponent, ABC):
    def __call__(self, step) -> dict:
        events: dict = self.events

        return events

    @property
    @abstractmethod
    def events(self) -> dict:
        pass
