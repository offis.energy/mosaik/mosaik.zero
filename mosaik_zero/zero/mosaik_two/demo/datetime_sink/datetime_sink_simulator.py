from mosaik_zero.zero.mosaik_two.api.simulator import Simulator
from mosaik_zero.zero.mosaik_two.demo.datetime_sink.datetime_sink_model \
    import DatetimeSinkModel
from mosaik_zero.zero.mosaik_two.demo.datetime_sink.\
    datetime_sink_simulator_meta import DATETIME_SINK_SIMULATOR_META


class DatetimeSinkSimulator(Simulator):
    def __init__(self):
        super().__init__(meta=DATETIME_SINK_SIMULATOR_META)
        self._model: DatetimeSinkModel = DatetimeSinkModel()

    def create(self, num, model, **model_params):
        created_model_instances: list = \
            [
                {
                    model: {
                        param: None
                        for param in self.meta['models'][model]['params']
                    },
                }
                for _ in range(num)
            ]

        return created_model_instances

    def step(self, time, inputs) -> int:
        self._model.step(inputs=inputs)

        return time + 1

    def get_data(self, outputs) -> dict:
        data: dict = {self._model.eid: self._model.get_data()}

        return data
