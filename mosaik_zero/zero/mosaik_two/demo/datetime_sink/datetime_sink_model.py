class DatetimeSinkModel(object):

    @property
    def eid(self):
        return type(self).__name__ + '-0'

    def get_data(self) -> dict:
        data: dict = {}

        return data

    def step(self, inputs):
        print(f'The date and time were {inputs["datetime"]}.')
