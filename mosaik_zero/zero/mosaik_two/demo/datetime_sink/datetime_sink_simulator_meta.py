from mosaik_zero.zero.mosaik_two.demo.datetime_sink.datetime_sink_model \
    import DatetimeSinkModel

DATETIME_SINK_SIMULATOR_META = {
    'models': {
        DatetimeSinkModel.__name__: {
            'public': True,
            'params': [],
            'attrs': [
                'datetime'
            ],
        },
    },
}
