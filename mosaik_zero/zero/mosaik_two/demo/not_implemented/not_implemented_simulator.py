from mosaik_zero.zero.mosaik_two.api.simulator import Simulator


class NotImplementedSimulator(Simulator):
    def create(self, num, model, **model_params):
        raise NotImplementedError()

    def step(self, time, inputs) -> int:
        raise NotImplementedError()

    def get_data(self, outputs) -> dict:
        raise NotImplementedError()
