from mosaik_zero.zero.mosaik_two.demo.datetime_source.datetime_source_model \
    import DatetimeSourceModel

DATETIME_SOURCE_SIMULATOR_META = {
    'models': {
        DatetimeSourceModel.__name__: {
            'public': True,
            'params': [],
            'attrs': [
                'datetime'
            ],
        },
    },
}
