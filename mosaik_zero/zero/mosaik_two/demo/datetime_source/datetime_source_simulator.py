from mosaik_zero.zero.mosaik_two.api.simulator import Simulator
from mosaik_zero.zero.mosaik_two.demo.datetime_source.datetime_source_model \
    import DatetimeSourceModel
from mosaik_zero.zero.mosaik_two.demo.datetime_source.\
    datetime_source_simulator_meta import DATETIME_SOURCE_SIMULATOR_META


class DatetimeSourceSimulator(Simulator):
    def __init__(self):
        super().__init__(meta=DATETIME_SOURCE_SIMULATOR_META)
        self._model: DatetimeSourceModel = DatetimeSourceModel()

    def create(self, num, model, **model_params):
        created_model_instances: list = \
            [
                {
                    model: {
                        param: None
                        for param in self.meta['models'][model]['params']
                    },
                }
                for _ in range(num)
            ]

        return created_model_instances

    def step(self, time, inputs) -> int:
        return time + 1

    def get_data(self, outputs) -> dict:
        data: dict = {self._model.eid: self._model.get_data()}

        return data
