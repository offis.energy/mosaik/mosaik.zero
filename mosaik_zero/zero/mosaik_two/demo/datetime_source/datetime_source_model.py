from datetime import datetime


class DatetimeSourceModel(object):

    @property
    def eid(self):
        return type(self).__name__ + '-0'

    def get_data(self) -> dict:
        date_and_time = datetime.utcnow()
        print(f'The date and time is {date_and_time}.')
        data: dict = {
            'datetime': date_and_time,
        }

        return data
