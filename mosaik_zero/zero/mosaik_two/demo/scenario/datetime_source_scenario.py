from mosaik_zero.zero.mosaik_two.api.entity import Entity
from mosaik_zero.zero.mosaik_two.demo.datetime_sink.datetime_sink_simulator \
    import DatetimeSinkSimulator
from mosaik_zero.zero.mosaik_two.demo.datetime_source.\
    datetime_source_simulator import DatetimeSourceSimulator
from mosaik_zero.zero.mosaik_two.api.world import World


def datetime_source_scenario():
    sim_config = {
        DatetimeSourceSimulator.__name__: {
            'python':
                DatetimeSourceSimulator.__module__ + ':' +
                DatetimeSourceSimulator.__name__
        },
        DatetimeSinkSimulator.__name__: {
            'python':
                DatetimeSinkSimulator.__module__ + ':' +
                DatetimeSinkSimulator.__name__
        },
    }

    world = World(sim_config=sim_config)

    datetime_source_simulator = world.start(
        simulator_name=DatetimeSourceSimulator.__name__,
    )
    datetime_source_model_mock = getattr(
        datetime_source_simulator,
        DatetimeSourceSimulator.__name__,
    )
    datetime_source_model: Entity = datetime_source_model_mock()

    datetime_sink_simulator = world.start(
        simulator_name=DatetimeSinkSimulator.__name__,
    )
    datetime_sink_model_mock = getattr(
        datetime_sink_simulator,
        DatetimeSinkSimulator.__name__,
    )
    datetime_sink_model: Entity = datetime_sink_model_mock()

    world.connect(
        src=datetime_source_model, dst=datetime_sink_model,
        attr_pairs=('datetime', 'datetime'),
    )

    end = 1
    world.run(until=end)

    world.shutdown()


if __name__ == '__main__':
    datetime_source_scenario()
