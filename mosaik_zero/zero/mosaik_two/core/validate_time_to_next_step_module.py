def validate_next_step_at(
    *,
    next_step_at: int,
    step: int,
) -> None:
    if not isinstance(next_step_at, int):
        raise TypeError(
            'Time of next step must be an integer, '
            f'but was "{type(next_step_at)}".'
        )
    if next_step_at < 1:
        raise ValueError(
            'Time of next step must be positive, '
            f'but found "{next_step_at}".'
        )
    if next_step_at < step:
        raise ValueError(
            f'Time of next step must be larger than current step of {step}, '
            f'but found "{next_step_at}".'
        )
