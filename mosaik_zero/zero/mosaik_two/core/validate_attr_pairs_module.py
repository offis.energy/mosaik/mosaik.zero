def validate_attr_pairs(attr_pairs):
    if not isinstance(attr_pairs, tuple):
        raise TypeError(
            f'attr_pairs were expected to be a tuple, '
            f'but got {type(attr_pairs)}.'
        )
