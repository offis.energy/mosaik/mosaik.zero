def validate_meta(meta):
    if not isinstance(meta, dict):
        raise TypeError(
            f'meta was expected to be a dict, but got {type(meta)}.'
        )

    if len(meta['models']) > 1:
        raise NotImplementedError(
            f'Only one model per simulator is implemented, '
            f'but got {len(meta["models"])} models: {meta["models"]}'
        )
