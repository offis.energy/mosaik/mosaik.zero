def validate_until(until):
    if not isinstance(until, int):
        raise TypeError(
            f'until was expected to be int, but got {type(until)}.'
        )
