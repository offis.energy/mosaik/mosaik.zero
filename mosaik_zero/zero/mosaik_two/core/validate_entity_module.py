from mosaik_zero.zero.mosaik_two.api.entity import Entity


def validate_entity(entity):
    if not isinstance(entity, Entity):
        raise TypeError(
            'entity was expected to be an Entity object, '
            f'but got {type(entity)}.'
        )
