def validate_simulator_name(simulator_name, sim_config):
    if not isinstance(simulator_name, str):
        raise TypeError(
            'simulator_name was expected to be str, '
            f'but got {type(simulator_name)}.'
        )
