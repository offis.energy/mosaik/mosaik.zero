def validate_sim_config(sim_config):
    """
    Validate simulation config

    :param sim_config:
    :return: None
    :raises: Exceptions if anything was wrong.
    """

    if not isinstance(sim_config, dict):
        raise TypeError(
            f'sim_config was expected to be a dict, but got {type(sim_config)}.'
        )
