import importlib
from typing import List

from mosaik_zero.zero.mosaik_two.api.simulator import Simulator
from mosaik_zero.zero.mosaik_two.core.validate_until_module import \
    validate_until
from mosaik_zero.zero.mosaik_two.core.validate_simulator_name_module import \
    validate_simulator_name
from mosaik_zero.zero.mosaik_two.core.validate_attr_pairs_module import \
    validate_attr_pairs
from mosaik_zero.zero.mosaik_two.core.validate_entity_module import \
    validate_entity
from mosaik_zero.zero.mosaik_two.core.validate_sim_config_module import \
    validate_sim_config
from mosaik_zero.zero.mosaik_zero.api.scenario.connection import Connection
from mosaik_zero.zero.mosaik_zero.api.scenario.simulate_module import simulate
from mosaik_zero.zero.mosaik_two.api.entity import Entity


class World(object):
    def __init__(self, sim_config):
        validate_sim_config(sim_config=sim_config)

        self._sim_config = sim_config

        self._simulators_names_and_instances: dict = {}
        self._connections: List = []

    def connect(
        self, src: Entity, dst: Entity, attr_pairs: tuple
    ) -> None:
        validate_entity(entity=src)
        validate_entity(entity=dst)
        validate_attr_pairs(attr_pairs=attr_pairs)

        # TODO Implement a factory to avoid modifying protected attributes.
        src._provisions.append(attr_pairs[0])
        dst._requirements.append(attr_pairs[1])

        connection = Connection(
            source_component=src,
            source_provision=attr_pairs[0],
            sink_component=dst,
            sink_requirement=attr_pairs[1],
        )
        self._connections.append(connection)

    def start(self, simulator_name) -> Simulator:
        validate_simulator_name(
            simulator_name=simulator_name,
            sim_config=self._sim_config,
        )

        simulator_module_name: str = \
            self._sim_config[simulator_name]['python'].split(':')[0]
        simulator_class: Simulator = \
            importlib.import_module(name=simulator_module_name,)

        return simulator_class

    def run(self, until):
        validate_until(until=until)

        simulate(
            connections=self._connections,
            limit_steps=until,
        )

    def shutdown(self):
        for simulator_instance in self._simulators_names_and_instances.values():
            simulator_instance.__del__()
