from abc import abstractmethod

from mosaik_zero.zero.mosaik_two.api.entity import Entity
from mosaik_zero.zero.mosaik_zero.api.component.sink_component import \
    SinkComponent
from mosaik_zero.zero.mosaik_zero.api.component.source_component import \
    SourceComponent
from mosaik_zero.zero.mosaik_two.core.validate_meta_module import \
    validate_meta
from mosaik_zero.zero.mosaik_two.core.validate_time_to_next_step_module \
    import validate_next_step_at


__version__ = '2.4.1'
__api_version__ = __version__


class Simulator(SinkComponent, SourceComponent, Entity):
    """
    This is the main interface for simulator and model creators.

    Inherit from this base class and implement its abstract API calls.
    """

    # TODO Should Simulator really inherit from Entity?

    def __init__(self, meta):
        super().__init__()

        validate_meta(meta)
        self._meta = {
            'api_version': '2.4.1',
            'models': {},  # TODO Is this really necessary?
        }
        self._meta.update(meta)

        self._mosaik = None  # Will be set by "start_simulation()"

        sid = type(self).__name__ + '-0'
        self.init(sid)
        self._next_step_at: int = 0

        self._data: dict = {}

        self._first_run = True

        self._provisions = []
        self._requirements = []

    @property
    def provisions(self) -> tuple:
        return tuple(self._provisions)

    @property
    def requirements(self) -> tuple:
        return tuple(self._requirements)

    @property
    def meta(self) -> dict:
        """
        Meta data describing the simulator (the same that is returned by
        :meth:`init()`).

        ::

            {
                'api_version': 'x.y',
                'models': {
                    'ModelName': {
                        'public': True|False,
                        'params': ['param_1', ...],
                        'attrs': ['attr_1', ...],
                        'any_inputs': True|False,
                    },
                    ...
                },
                'extra_methods': [
                    'do_cool_stuff',
                    'set_static_data'
                ]
            }

        The *api_version* is a string that defines which version of the
        mosaik API
        the simulator implements.  Since mosaik API version 2.3, the simulator's
        `major version <http://semver.org/>`_ ("x", in the snippet above) has
        to be
        equal to mosaik's.  Mosaik will cancel the simulation if a version
        mismatch
        occurs.

        *models* is a dictionary describing the models provided by this
        simulator.
        The entry *public* determines whether a model can be instantiated by
        a user
        (``True``) or if it is a sub-model that cannot be created directly
        (``False``). *params* is a list of parameter names that can be passed to
        the model when creating it. *attrs* is a list of attribute names that
        can
        be accessed (reading or writing).  If the optional *any_inputs* flag
        is set
        to ``true``, any attributes can be connected to the model, even if
        they are
        not *attrs*. This may, for example, be useful for databases that
        don't know
        in advance which attributes of an entity they'll receive.


        *extra_methods* is an optional list of methods that a simulator
        provides in
        addition to the standard API calls (``init()``, ``create()`` and so on).
        These methods can be called while the scenario is being created and
        can be
        used for operations that don't really belong into ``init()`` or
        ``create()``.

        """
        return self._meta

    @property
    def mosaik(self):
        """
        An RPC proxy to mosaik.

        Not actually included in this implementation.
        """

        raise NotImplementedError(
            'This implementation does not include a mosaik RPC proxy.'
        )

    @mosaik.setter
    def mosaik(self, mosaik):
        self._mosaik = mosaik

    def __call__(self, step, *inputs) -> dict:
        if not inputs:
            inputs = {}
        if isinstance(inputs, tuple):
            if len(inputs) != 1:
                raise ValueError(
                    'Only one input supported, but '
                    f'got {len(inputs)} inputs: {inputs}'
                )
            inputs = inputs[0]

        if self._next_step_at > step:
            # Repeat last data when not stepped.
            return self._data

        self._next_step_at: int = self.step(time=step, inputs=inputs)
        validate_next_step_at(next_step_at=self._next_step_at, step=step)

        if self._first_run:
            model = list(self._meta['models'].keys())[0]
            num = 1
            params = {}
            self.create(num, model, **params)

            self.setup_done()

            self._first_run = False

        eid = 'mosaik_zero'
        outputs = {
            eid: self.provisions,
        }
        self._data: dict = self.get_data(outputs=outputs)
        # Unpackage mosaik 2 simulator name key
        self._data: dict = self._data[list(self._data.keys())[0]]
        return self._data

    def __del__(self):
        self.finalize()

    def init(self, sid, **sim_params) -> dict:
        """
        Initialize the simulator with the ID *sid* and apply additional
        parameters *(sim_params)* sent by mosaik. Return the meta data
        :attr:`meta`.

        If your simulator has no *sim_params*, you don't need to override this
        method.
        """
        return self.meta

    @abstractmethod
    def create(self, num, model, **model_params):
        """
        Create *num* instances of *model* using the provided *model_params*.

        *num* is an integer for the number of model instances to create.

        *model* needs to be a public entry in the simulator's
        ``meta['models']``.

        *model_params* is a dictionary mapping parameters (from
        ``meta['models'][model]['params']``) to their values.

        Return a (nested) list of dictionaries describing the created model
        instances (entities). The root list must contain exactly *num*
        elements. The number of objects in sub-lists is not constrained::

            [
                {
                    'eid': 'eid_1',
                    'type': 'model_name',
                    'rel': ['eid_2', ...],
                    'children': [
                        {'eid': 'child_1', 'type': 'child'},
                        ...
                    ],
                },
                ...
            ]

        The entity ID (*eid*) of an object must be unique within a simulator
        instance. For entities in the root list, *type* must be the same as the
        *model* parameter. The type for objects in sub-lists may be anything
        that can be found in ``meta['models']``. *rel* is an optional list of
        related entities; "related" means that two entities are somehow connect
        within the simulator, either logically or via a real data-flow (e.g.,
        grid nodes are related to their adjacent branches). The *children*
        entry is optional and may contain a sub-list of entities.
        """
        raise NotImplementedError(
            'Method create() was not implemented when subclassing Simulator().'
        )

    def setup_done(self) -> None:
        """
        Callback that indicates that the scenario setup is done and the actual
        simulation is about to start.

        At this point, all entities and all connections between them are know
        but no simulator has been stepped yet.

        Implementing this method is optional.

        *Added in mosaik API version 2.3*
        """
        pass

    @abstractmethod
    def step(self, time, inputs) -> int:
        """
        Perform the next simulation step from time *time* using input values
        from *inputs* and return the new simulation time (the time at which
        ``step()`` should be called again).

        *time* and the time returned are integers. Their unit has to agree to
        that used in the specific mosaik scenario, e.g. *seconds* (from
        simulation start).

        *inputs* is a dict of dicts mapping entity IDs to attributes and
        dicts of values (each simulator has do decide on its own how to reduce
        the values (e.g., as its sum, average or maximum)::

            {
                'dest_eid': {
                    'attr': {'src_fullid': val, ...},
                    ...
                },
                ...
            }
        """
        raise NotImplementedError(
            'Method create() was not implemented when subclassing Simulator().'
        )

    @abstractmethod
    def get_data(self, outputs) -> dict:
        """
        Return the data for the requested attributes in *outputs*

        *outputs* is a dict mapping entity IDs to lists of attribute names
        whose values are requested::

            {
                'eid_1': ['attr_1', 'attr_2', ...],
                ...
            }

        The return value needs to be a dict of dicts mapping entity IDs and
        attribute names to their values::

            {
                'eid_1: {
                    'attr_1': 'val_1',
                    'attr_2': 'val_2',
                    ...
                },
                ...
            }

        """
        pass

    def configure(self, args, backend, env) -> None:
        """
        This was meant for simpy. This implementation will ignore it.
        """
        raise NotImplementedError(
            'Method configure() is not implemented in this Simulator() class.'
        )

    def finalize(self) -> None:
        """
        This method can be overridden to do some clean-up operations after
        the simulation finished (e.g., shutting down external processes).
        """
        pass
