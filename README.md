# mosaik.Zero

A toy implementation of mosaik from scratch.

## Status

[![pipeline status](https://gitlab.com/offis.energy/mosaik/mosaik.zero/badges/master/pipeline.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.zero/-/pipelines)
[![coverage report](https://gitlab.com/offis.energy/mosaik/mosaik.zero/badges/master/coverage.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.zero/-/jobs)
